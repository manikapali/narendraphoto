<?php
/**
 * Created by PhpStorm.
 * User: Samit
 * Date: 8/5/2015
 * Time: 12:44 PM
 */
get_header();
if ( have_posts() ):
	?>
	<div class="gallery-block">
		<div class="container clearfix">
			<?php the_title( '<h1>', '</h1>' ) ?>
			<div class="img-list-block">
				<ul class="gallery-detail-img img-category-block clearfix">
					<?php while ( have_posts() ):
						the_post();
						$galleries = get_post_meta( get_the_ID(), '_gallery_images_list', 1 );
						if ( ! empty( $galleries ) ):
							foreach ( $galleries as $galleries_id => $galleries_url ):
								?>
								<li class="img-thumb">
									<div class="img-holder">
										<?php echo wp_get_attachment_image( $galleries_id, array( 277, 285 ) ); ?>
										<div class="overlay-transp">
											<a href="<?php echo get_attachment_link( $galleries_id ); ?>"
											   class="hover-icon icon-plus"></a>
											<a href="<?php echo esc_url( $galleries_url ); ?>" data-lightbox="image-1"
											   class="hover-icon icon-search "></a>
										</div>
									</div>
								</li>
							<?php endforeach;
						endif;
					endwhile; ?>
				</ul>
			</div>
			<!-- img-list-block ends-->
		</div>
	</div>
<?php endif; ?>
<!-- gallery-block ends -->
<?php get_footer(); ?>
