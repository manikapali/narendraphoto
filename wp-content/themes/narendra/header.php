<?php
/**
 * header.php
 *
 *
 * The header for the theme.
 */
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo( 'name' ); ?></title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<?php wp_head(); ?>
	<script charset="utf-8" type="text/javascript">var switchTo5x = true;</script>
	<script charset="utf-8" type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
	<script charset="utf-8" type="text/javascript">stLight.options({
			"publisher": "wp.866cbadd-fbe3-49fd-9098-e25d72078ea9",
			"doNotCopy": false,
			"hashAddressBar": true,
			"doNotHash": false
		});
		var st_type = "wordpress4.2.4";</script>
</head>
<body <?php
if ( is_front_page() ) {
	body_class( 'home' );
} elseif ( is_archive() ) {
	body_class( 'page gallery' );
} else {
	body_class( 'page' );
}
?>>
<div id="header">
	<div class="container">
		<div class="header-top clearfix">
			<div class="main-logo no-menu">
				<a href="/"><h1>Narendra Shrestha <span>Photo Journalist</span></h1></a>
			</div>
			<!-- main-logo ends -->
			<div class="main-nav">
				<div class="main-nav-share btn">
					<a href="#" class="icon-share"></a>
					<ul>
						<?php do_action( 'mok_social_account' ); ?>
					</ul>
				</div>
				<div class="main-nav-menu btn"><a href="#" class="icon-menu">Menu</a>
					<span class="nav-close">X</span>
					<?php do_action( 'mok_desktop_menu' ); ?>
				</div>
			</div>
			<!-- main-nav ends -->
		</div>
	</div>
</div>
<!-- header ends -->