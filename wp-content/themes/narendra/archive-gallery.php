<?php
/**
 * Created by PhpStorm.
 * User: Samit
 * Date: 8/5/2015
 * Time: 11:46 AM
 */
get_header();
?>
	<div class="gallery-block">
		<div class="container clearfix">
			<h1>Gallery</h1>
			<?php if ( have_posts() ): ?>
				<div class="img-list-block">
					<?php
					$count = 0;
					while ( have_posts() ):
						the_post();
						?>
						<?php
						$galleries = get_post_meta( get_the_ID(), '_gallery_images_list', 1 );
						if ( is_array( $galleries ) ):
							$galleries = array_slice( $galleries, 0, 8, true );
							$count ++;
							?>
							<ul class="img-category-block clearfix">
								<li class="list-title">
									<a href="<?php the_permalink(); ?>"><h1>Works
											<span><?php printf( '%02d', $count ); ?></span> <span
												class="list-title-proper"><?php the_title(); ?></span></h1></a>
								</li>
								<?php
								foreach ( $galleries as $galleries_id => $galleries_url ):
									?>
									<li class="img-holder">
										<a href="<?php echo esc_url( $galleries_url ); ?>"
										   data-lightbox="gallery-img1">
											<?php echo wp_get_attachment_image( $galleries_id, array(
												221,
												227
											) ); ?>
											<div class="overlay-transp">
												<span class="hover-icon icon-search"></span>
											</div>
										</a>
									</li>
									<?php
								endforeach;
								?>
								<li class="list-detail">
									<a href="<?php the_permalink(); ?>"><?php echo count( $galleries ) > 6 ? "View More" : "View Gallery"; ?></a>
								</li>
							</ul>
							<?php
						endif;
					endwhile; ?>
				</div>
			<?php endif; ?>
			<!-- img-list-block ends-->
		</div>
	</div>
	<!-- gallery-block ends -->
<?php get_footer(); ?>