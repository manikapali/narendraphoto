<?php
/**
 * 404.php
 *
 * The template for displaying 404 pages.
 */
get_header();
?>
    <div class="error-404">
        <div class="container">
            <h1><?php _e('404 ERROR', '_mok'); ?></h1>a
        <h2><?php _e('The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.', '_mok'); ?></h2>

        <p><?php _e('Please try the following:', '_mok'); ?></p>
        <ul>
            <li><?php _e('Check your spelling', '_mok'); ?></li>
            <li><?php printf(__('Return to the <a href="%s">home page</a>', '_mok'), home_url()); ?></li>
            <li><?php _e('Click the <a href="javascript:history.back()">Back</a> button', '_mok'); ?></li>
        </ul>
    </div></div>
<?php get_footer(); ?>