<?php
/**
 * Created by PhpStorm.
 * User: Samit
 * Date: 8/5/2015
 * Time: 9:09 PM
 */
get_header();
?>
	<div class="gallery-block">
		<div class="container clearfix">
			<h1><?php the_title(); ?></h1>

			<div class="social-widget-block">
				<div class="social-widget fb-widget">
					<div class="fb-like" data-href="<?php the_permalink(); ?>"
					     data-layout="button_count"
					     data-action="like" data-show-faces="true">
					</div>
				</div>
				<div class="social-widget twt-widget">
					<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
					<script>!function (d, s, id) {
							var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
							if (!d.getElementById(id)) {
								js = d.createElement(s);
								js.id = id;
								js.src = p + '://platform.twitter.com/widgets.js';
								fjs.parentNode.insertBefore(js, fjs);
							}
						}(document, 'script', 'twitter-wjs');</script>
				</div>
			</div>
			<div class="detail-img-wrapper">
				<?php if ( have_posts() ): ?>
					<div class="detail-img-holder">
						<?php while ( have_posts() ): the_post(); ?>
							<div class="img-holder">
								<?php echo wp_get_attachment_image( get_the_ID(), array( 556, 528 ) ); ?>
							</div>
							<div class="social-links-inner clearfix">
								<h2>SHARE</h2>
								<span class='st_facebook_large' st_title='<?php the_title(); ?>'
								      st_url='<?php the_permalink(); ?>'></span>
								<span class='st_twitter_large' st_title='<?php the_title(); ?>'
								      st_url='<?php the_permalink(); ?>'></span>
								<span class='st_linkedin_large' st_title='<?php the_title(); ?>'
								      st_url='<?php the_permalink(); ?>'></span>
								<span class='st_pinterest_large' st_title='<?php the_title(); ?>'
								      st_url='<?php the_permalink(); ?>'></span>
							</div>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
				<div class="fb-comments" data-href="http://developers.facebook.com/docs/plugins/comments/"
				     data-numposts="1"></div>
			</div>
		</div>
	</div>
	<!-- gallery-block ends -->
<?php get_footer(); ?>