<?php
/**
 * Created by PhpStorm.
 * User: msi
 * Date: 8/3/2015
 * Time: 11:59 AM
 */
require 'framework/index.php';

use Moksha\Trekking\ZurbOffCanvasWalker;
use Moksha\Trekking\ZurbWalker;

/**
 * Define constants
 */
define( 'THEME_BASE', get_stylesheet_directory_uri() );
define( 'STYLES', THEME_BASE . '/assets/css' );
define( 'IMAGES', THEME_BASE . '/assets/images' );
define( 'SCRIPTS', THEME_BASE . '/assets/js' );
define( 'VENDORS', THEME_BASE . '/assets/vendor' );


if ( ! isset( $content_width ) ) {
	$content_width = 800;
}

/**
 * Set up theme default and register various supported features
 */
if ( ! function_exists( 'mok_setup' ) ) {
	function mok_setup() {
		//Make the theme available for translation.
		$lang_dir = THEME_BASE . '/lang';
		load_theme_textdomain( '_mok', $lang_dir );

		//Add support for automatic feed links.
		add_theme_support( 'automatic-feed-links' );

		//Add support for post thumbnails.
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'fd-lrg', 1024, 99999 );
		add_image_size( 'fd-med', 768, 99999 );
		add_image_size( 'fd-sm', 320, 9999 );

		//Add support for post formats.
		add_theme_support( 'post-formats', array(
			'aside',
			'gallery',
			'link',
			'image',
			'quote',
			'status',
			'video',
			'audio',
			'chat'
		) );

		//Add support for menus.
		add_theme_support( 'menus' );
		register_nav_menus(
			array(
				'main-menu'   => __( 'Main Menu', '_mok' ),
				'footer-menu' => __( 'Footer Menu', '_mok' )
			)
		);

		// enqueue base scripts and styles
		add_action( 'wp_enqueue_scripts', 'mok_scripts_and_styles', 999 );

		//Register the widget areas
		add_action( 'widgets_init', 'mok_widget_init' );

	}
}
add_action( 'after_setup_theme', 'mok_setup' );

if ( ! function_exists( 'mok_widget_init' ) ) {
	function mok_widget_init() {
		if ( function_exists( 'register_sidebar' ) ) {
			register_sidebar(
				array(
					'name'          => __( 'Main Widget Area', '_mok' ),
					'id'            => 'main-sidebar',
					'description'   => __( 'Appears on sidebar of posts and pages.', '_mok' ),
					'before_widget' => '<div class="widget">',
					'after_widget'  => '</div> <!-- end widget -->',
					'before_title'  => '<div class="widget-title">',
					'after_title'   => '</div>'
				)
			);
			register_sidebar(
				array(
					'name'          => __( 'Footer Widget Area', '_mok' ),
					'id'            => 'footer-widget',
					'description'   => __( 'Appears on footer of all pages.', '_mok' ),
					'before_widget' => '<div class="widget">',
					'after_widget'  => '</div> <!-- end widget -->',
					'before_title'  => '<div class="widget-title">',
					'after_title'   => '</div>'
				)
			);
		}
	}
}

/**
 * Display meta information for a specific post
 */
if ( ! function_exists( 'mok_entry_meta' ) ) {
	function mok_entry_meta() {
		echo '<span class="byline author">' . __( 'Written by', '_mok' ) . ' <a href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '" rel="author" class="fn">' . get_the_author() . ', </a></span>';
		echo '<time class="updated" datetime="' . get_the_time( 'c' ) . '" pubdate>' . get_the_time( 'F jS, Y' ) . '</time>';
	}
};

/**
 * Loads stylesheet and scripts
 */
if ( ! function_exists( 'mok_scripts_and_styles ' ) ) {
	function mok_scripts_and_styles() {
		if ( ! is_admin() ) {
			wp_register_script( 'mok-modernizr', VENDORS . '/modernizr/modernizr.js', array(), '2.6.2', false );
			wp_register_script( 'mok-foundation-js', VENDORS . '/foundation/js/foundation.min.js', array( 'jquery' ), '', true );
			wp_register_script( 'mok-plugins-js', SCRIPTS . '/plugins.js', array( 'jquery' ), false, true );
			wp_register_script( 'mok-js', SCRIPTS . '/scripts.js', array(
				'jquery',
				'mok-foundation-js',
				'mok-plugins-js'
			), false, true );
			wp_localize_script( 'mok-js', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

			if ( get_option( 'thread_comments' ) ) {
				wp_enqueue_script( 'comment-reply' );
			}
			wp_enqueue_style( 'style', STYLES . '/style.css' );
			wp_enqueue_style( 'normalize', STYLES . '/normalize.css' );
			wp_enqueue_style( 'fonts', STYLES . '/fonts.css' );
			wp_enqueue_style( 'slick', STYLES . '/slick.css' );
			wp_enqueue_style( 'lightbox', STYLES . '/lightbox.css' );
			wp_enqueue_script( 'jquery-1.9.1.min', SCRIPTS . '/jquery-1.9.1.min.js', array( 'jquery' ), false, true );
			wp_enqueue_script( 'plugins', SCRIPTS . '/plugins.js', array( 'jquery' ), false, true );
			wp_enqueue_script( 'script', SCRIPTS . '/script.js', array( 'jquery' ), false, true );

		}
	}
}

function remove_contact_form_style() {
	return false;
}

add_filter( 'wpcf7_load_css', 'remove_contact_form_style', 1 );

if ( ! function_exists( 'mok_desktop_menu' ) ) {
	function mok_desktop_menu() {
		if ( has_nav_menu( 'main-menu' ) ) {
			wp_nav_menu( array(
				'theme_location' => 'main-menu',
				'container'      => false,
				'depth'          => 0,
				'menu_class'     => '',
				'items_wrap'     => '<ul><li class="main-logo">
                            <a href="/"><h1>Narendra Shrestha <span>Photo Journalist</span></h1></a>
                        </li>%3$s</ul>'
			) );
		}
	}
}
add_action( 'mok_desktop_menu', 'mok_desktop_menu' );

function modify_social_fb_class() {
	return 'btn btn-social hover-fb icon-facebook';
}

add_filter( 'mok_social_fb_class', 'modify_social_fb_class' );

function modify_social_tw_class() {
	return 'btn btn-social hover-tw icon-twitter';
}

add_filter( 'mok_social_tw_class', 'modify_social_tw_class' );

function modify_social_ln_class() {
	return 'btn btn-social hover-ln icon-linkedin';
}

add_filter( 'mok_social_ln_class', 'modify_social_ln_class' );

function modify_social_pt_class() {
	return 'btn btn-social hover-pt icon-pinterest';
}

add_filter( 'mok_social_pt_class', 'modify_social_pt_class' );