<?php
/**
 * Template Name: Page: About Me
 * Created by PhpStorm.
 * User: Samit
 * Date: 8/4/2015
 * Time: 5:28 PM
 */
get_header();

?>
<div class="gallery-block">
	<div class="container clearfix">
		<?php if ( have_posts() ): ?>
			<h1 class="story-title"><?php the_title(); ?></h1>
			<?php while ( have_posts() ): the_post(); ?>
				<div class="about-detail-wrapper clearfix">
					<?php if ( has_post_thumbnail() ): ?>
						<div class="about-img-wrapper">
							<div class="img-holder">
								<a href="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' )[0]; ?>"
								   data-lightbox="about-me-img">
									<?php the_post_thumbnail(); ?>
									<div class="overlay-transp">
										<span class="hover-icon icon-search"></span>
									</div>
								</a>
							</div>
							<div class="social-links-inner about-me-social clearfix">
								<ul>
									<?php do_action( 'mok_social_account' ); ?>
								</ul>
							</div>
							<!-- about-me-social ends -->
						</div>
					<?php endif; ?>
					<!-- about-img-wrapper ends -->
					<div class="stories-detail">
						<?php the_content(); ?>
					</div>
					<!-- stories-detail ends -->
				</div>
			<?php endwhile;
		endif; ?>
		<!-- about-detail-wrapper ends -->
	</div>
</div>

<!-- gallery-block ends -->
<?php get_footer(); ?>
