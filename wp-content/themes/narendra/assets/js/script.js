jQuery(document).ready(function () {

    /*js for menu*/
    jQuery('.icon-menu').click(function(){
        jQuery('body').addClass('open');
        jQuery('.main-nav-menu ul').addClass('open');
    });

    jQuery('.nav-close').click(function(){
        jQuery('body').removeClass('open');
        jQuery('.main-nav-menu ul').removeClass('open');
    });

    jQuery('.main-nav-share .icon-share').click(function(){
        jQuery('body').addClass('open');
        jQuery('.main-nav-share ul').addClass('open');
    });

    jQuery('.nav-close').click(function(){
        jQuery('body').removeClass('open');
        jQuery('.main-nav-share ul').removeClass('open');
    });

    /*js for homepage slider*/
    if(jQuery('.featured-img-slides').length > 0){
    jQuery('.featured-img-slides').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        infinite: true,
        //autoplay: true,
        autoplaySpeed: 3000,
        arrow: true,
        pauseOnHover: true,
        prevArrow: '<button type="button" class="slick-prev icon-left-dir"></button>',
        nextArrow: '<button type="button" class="slick-next icon-right-dir"></button>',

        responsive: [
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },

            {
                breakpoint: 321,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },

            {
                breakpoint: 481,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    }

    /*js for story detail slider*/
    $('.story-detail-img').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        swipeToSlide: true,
        dots: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    //if(jQuery('.gallery .img-category-block').length > 0) {
    //    jQuery('.gallery .img-category-block').slick({
    //        slidesToShow: 1,
    //        slidesToScroll: 1,
    //        arrows: true,
    //        infinite: true,
    //        //autoplay: true,
    //        autoplaySpeed: 3000,
    //        arrow: true,
    //        pauseOnHover: true,
    //        prevArrow: '<button type="button" class="slick-next icon-left-open"></button>',
    //        nextArrow: '<button type="button" class="slick-next icon-right-open"></button>',
    //
    //        responsive: [
    //            {
    //                breakpoint: 768,
    //                settings: {
    //                    slidesToShow: 2,
    //                    slidesToScroll: 2
    //                }
    //            },
    //            {
    //                breakpoint: 320,
    //                settings: {
    //                    slidesToShow: 1,
    //                    slidesToScroll: 1
    //                }
    //            }
    //            // You can unslick at a given breakpoint now by adding:
    //            // settings: "unslick"
    //            // instead of a settings object
    //        ]
    //    });
    //}

// js for equal height
    jQuery('.img-category-block').each(function() {
        jQuery(this).children('.img-category-block li').matchHeight({
            byRow: false
        });
    });
});
