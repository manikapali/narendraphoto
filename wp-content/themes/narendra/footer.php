<?php
/**
 * footer.php
 *
 *
 * The template for displaying the footer.
 */
?>
<div id="footer">
	<div class="container clearfix">
		<div class="footer-top clearfix">
			<div class="contact-links">
				<?php do_action( 'mok_contact_info' ); ?>
			</div>
			<!-- contact-links ends-->
			<div class="contact-form-block">
				<h2>Get In Touch</h2>
				<?php echo do_shortcode( '[contact-form-7 id="37" title="Contact form 1"]' ); ?>
			</div>
			<!-- contact-form-block ends -->
		</div>
		<!-- footer-top ends -->
		<div class="footer-down">
			<p>&copy; Copyright <?php echo date( "Y" ); ?> Narendra Shrestha. All Rights Reserved</p>
		</div>
		<!-- footer-down ends -->
	</div>
</div>
<?php wp_footer(); ?>
</body>
</html>