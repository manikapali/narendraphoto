<?php
/**
 * Created by PhpStorm.
 * User: Samit
 * Date: 8/4/2015
 * Time: 12:53 PM
 */
get_header();
if ( have_posts() ):
	?>
	<div class="gallery-block">
		<div class="container clearfix">
			<?php the_title( '<h1 class="story-title">', '</h1>' ); ?>
			<div class="social-widget-block">
				<script>(function (d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;
						js = d.createElement(s);
						js.id = id;
						js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
						fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
				<script>(function (d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;
						js = d.createElement(s);
						js.id = id;
						js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
						fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
				<div class="social-widget fb-widget">
					<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/"
					     data-layout="button_count" data-action="like" data-show-faces="true">
					</div>
				</div>
				<div class="social-widget twt-widget">
					<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
					<script>!function (d, s, id) {
							var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
							if (!d.getElementById(id)) {
								js = d.createElement(s);
								js.id = id;
								js.src = p + '://platform.twitter.com/widgets.js';
								fjs.parentNode.insertBefore(js, fjs);
							}
						}(document, 'script', 'twitter-wjs');</script>
				</div>
			</div>
			<!-- social-widget-block ends -->
			<div class="detail-wrapper">
				<ul class="story-detail-img img-category-block clearfix">
					<?php while ( have_posts() ):
						the_post();
						$stories = get_post_meta( get_the_ID(), '_stories_images_list', 1 );
						if ( ! empty( $stories ) ):
							foreach ( $stories as $stories_id => $stories_url ):
								?>
								<li class="img-thumb stories-thumb">
									<div class="img-holder">
										<a href="<?php echo esc_url( $stories_url ); ?>"
										   data-lightbox="detail-story-img">
											<?php echo wp_get_attachment_image( $stories_id, array( 276, 284 ) ); ?>
											<div class="overlay-transp">
												<span class="hover-icon icon-search "></span>
											</div>
										</a>
									</div>
								</li>
							<?php endforeach;
						endif;
					endwhile; ?>
				</ul>
				<div class="stories-detail">
					<?php the_content(); ?>
				</div>
			</div>
			<!-- detail-wrapper ends -->
			<div class="social-links-inner clearfix">
				<h2>SHARE</h2>
				<span class='st_facebook_large' st_title='<?php the_title(); ?>'
				      st_url='<?php the_permalink(); ?>'></span>
				<span class='st_twitter_large' st_title='<?php the_title(); ?>'
				      st_url='<?php the_permalink(); ?>'></span>
				<span class='st_linkedin_large' st_title='<?php the_title(); ?>'
				      st_url='<?php the_permalink(); ?>'></span>
				<span class='st_pinterest_large' st_title='<?php the_title(); ?>'
				      st_url='<?php the_permalink(); ?>'></span>
			</div>
			<!-- social-links-inner ends -->
		</div>
	</div>
<?php endif; ?>
	<!-- gallery-block ends -->
<?php get_footer(); ?>