<?php
/**
 * Created by PhpStorm.
 * User: Samit
 * Date: 8/4/2015
 * Time: 3:38 PM
 */
get_header();
?>
<?php
$args      = array(
	'post_type'      => 'billboard',
	'posts_per_page' => 1,
	'orderby'        => 'rand',
	'post_status'    => 'publish'
);
$billboard = new  WP_Query( $args );
if ( $billboard->have_posts() ):
	while ( $billboard->have_posts() ):
		$billboard->the_post(); ?>
		<div id="billboard">
			<div class="img-holder"><?php the_post_thumbnail( 'full' ); ?></div>
			<div class="container">
				<?php
				$billboard_detail  = get_post_meta( get_the_ID(), '_billboard_images_detail', 1 );
				$billboard_gallery = get_post_meta( get_the_ID(), '_billboard_gallery_link', 1 );
				?>
				<div class="header-title">
					<?php the_title( '<h1>', '</h1>' ); ?>
					<?php if ( ! empty( $billboard_detail ) ): ?>
						<a href="<?php echo esc_url( $billboard_detail ); ?>"
						   class="link-detail icon-right-dir btn">Details</a>
					<?php endif; ?>
					<?php if ( ! empty( $billboard_gallery ) ): ?>
						<a href="<?php echo esc_url( $billboard_gallery ); ?>"
						   class="link-to-gallery icon-right-dir btn">View
							My Galleries</a>
					<?php endif; ?>
				</div>
				<!-- header-title ends -->
			</div>
		</div>
		<?php
	endwhile;
endif;
?>
	<!-- billboard ends -->
	<div class="content-block1">
		<div class="container">
			<div class="img-list-block">
				<?php
				$args      = array(
					'post_type'      => 'gallery',
					'posts_per_page' => - 1,
					'orderby'        => 'rand',
					'post_status'    => 'publish'
				);
				$galleries = new  WP_Query( $args );
				$count     = 0;
				if ( $galleries->have_posts() ):
					while ( $galleries->have_posts() ):
						$galleries->the_post();

						?>
						<?php
						$gallery_list = get_post_meta( get_the_ID(), '_gallery_images_list', 1 );
						if ( is_array( $gallery_list ) ):
							$gallery_image_list = array_slice( $gallery_list, 0, 8, true );
							$count ++;
							?>
							<ul class="img-category-block clearfix">
								<li class="list-title">
									<a href="<?php the_permalink(); ?>"><h1>Works
											<span><?php printf( '%02d', $count ); ?></span> <span
												class="list-title-proper"><?php the_title(); ?></span></h1></a>
								</li>
								<?php
								foreach ( $gallery_image_list as $gallery_list_id => $gallery_list_url ):
									?>
									<li class="img-holder">
										<a href="<?php echo esc_url( $gallery_list_url ); ?>"
										   data-lightbox="gallery-img1">
											<?php echo wp_get_attachment_image( $gallery_list_id, array(
												232,
												239
											) ); ?>
											<div class="overlay-transp">
												<span class="hover-icon icon-search"></span>
											</div>
										</a>
									</li>
								<?php endforeach;
								?>
								<li class="list-detail">
									<a href="<?php the_permalink(); ?>"><?php echo count( $gallery_list ) > 8 ? "View More" : "View Gallery"; ?></a>
								</li>
							</ul>
							<?php
							if ( $count > 2 ):
								break;
							endif;
						endif;
					endwhile;
				endif; ?>
			</div>
			<!-- img-list-block ends-->
			<div class="featured-img-block">
				<?php
				$args    = array(
					'post_type'      => 'stories',
					'posts_per_page' => 5,
					'post_status'    => 'publish'
				);
				$stories = new WP_Query( $args );
				?>
				<h1>Featured Stories</h1>
				<?php if ( $stories->have_posts() ): ?>
					<ul class="featured-img-slides clearfix">
						<?php while ( $stories->have_posts() ):
							$stories->the_post(); ?>
							<li>
								<a href="<?php the_permalink(); ?>">
									<?php if ( has_post_thumbnail() ) {
										the_post_thumbnail( array( 390, 239 ) );
									}
									the_title( '<h3>', '</h3>' );
									?>
								</a>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
			<!-- featured-img-block ends -->
		</div>
	</div>
	<!-- content-block1 ends -->
	<div class="content-block2">
		<?php
		$about = new WP_Query( 'page_id=31' );
		if ( $about->have_posts() ):
			?>
			<div class="container">
				<?php while ( $about->have_posts() ):
					$about->the_post(); ?>
					<div class="homepage-about">
						<?php the_title( '<h1>', '</h1>' );
						the_content();
						?>
					</div>
				<?php endwhile; ?>
				<!-- homepage-about ends -->
				<div class="about-img">
					<img src="<?php echo IMAGES; ?>/photographer_img.jpg" alt="Photographer Image"/>

					<h1>Narendra Shrestha<span>Photo Journalist</span></h1>
				</div>
				<!-- about-img ends -->
			</div>
		<?php endif; ?>
	</div>
	<!-- content-block2-->
<?php get_footer(); ?>