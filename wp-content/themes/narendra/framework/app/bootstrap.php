<?php

/*
|--------------------------------------------------------------------------
| Post Type Initialization
|--------------------------------------------------------------------------
|
| Initialize class for generation of post type
|
*/
new Moksha\Trekking\Monkey\PostType();

/*
|--------------------------------------------------------------------------
| Post Meta Initialization
|--------------------------------------------------------------------------
|
| Initialize class for generation of metaboxes for different post type
|
*/
new Moksha\Trekking\Monkey\PostMeta();

/*
|--------------------------------------------------------------------------
| Tax Meta Initialization
|--------------------------------------------------------------------------
|
| Initialize class for generation of metaboxes for different taxonomy
|
*/
new Moksha\Trekking\Monkey\TaxMeta();

