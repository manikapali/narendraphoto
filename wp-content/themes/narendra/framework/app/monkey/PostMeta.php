<?php

namespace Moksha\Trekking\Monkey;


class PostMeta {

	/**
	 * PostMeta constructor.
	 */
	public function __construct() {
		add_action( 'init', array( &$this, 'init' ), 99 );
	}

	public function init() {
		add_filter( 'cmb2_meta_boxes', array( &$this, 'setPostMeta' ), 9999 );
	}

	public function setPostMeta() {
		$meta_boxes = array();

		return array_values( array_merge_recursive( $meta_boxes, apply_filters( 'mok_add_post_meta_boxes', array() ) ) );
	}
}