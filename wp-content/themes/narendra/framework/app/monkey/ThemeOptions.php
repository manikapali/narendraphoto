<?php

if ( ! class_exists( 'Redux' ) ) {
	return;
}

$opt_name = "moksha_option";

add_action( 'redux/loaded', 'remove_demo' );

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
	'opt_name'             => $opt_name,
	'display_name'         => $theme->get( 'Name' ),
	'display_version'      => $theme->get( 'Version' ),
	'menu_type'            => 'menu',
	'allow_sub_menu'       => true,
	'menu_title'           => __( 'Theme Options', '_mok' ),
	'page_title'           => __( 'Theme Options', '_mok' ),
	'google_api_key'       => '',
	'google_update_weekly' => false,
	'async_typography'     => true,
	'admin_bar'            => true,
	'admin_bar_icon'       => 'dashicons-admin-generic',
	'admin_bar_priority'   => 50,
	'global_variable'      => 'mok_option',
	'dev_mode'             => false,
	'update_notice'        => false,
	'customizer'           => true,
	'open_expanded'        => false,
	'page_priority'        => null,
	'page_parent'          => 'themes.php',
	'page_permissions'     => 'manage_options',
	'menu_icon'            => '',
	'last_tab'             => '',
	'page_icon'            => 'icon-themes',
	'page_slug'            => 'moksha_options',
	'save_defaults'        => true,
	'default_show'         => false,
	'default_mark'         => '',
	'show_import_export'   => true,
	'transient_time'       => 60 * MINUTE_IN_SECONDS,
	'output'               => true,
	'output_tag'           => true,
	'footer_credit'        => 'Theme Option by Moksha Design Studio',
	'database'             => '',
	'hints'                => array(
		'icon'          => 'icon-question-sign',
		'icon_position' => 'right',
		'icon_color'    => 'lightgray',
		'icon_size'     => 'normal',
		'tip_style'     => array(
			'color'   => 'light',
			'shadow'  => true,
			'rounded' => false,
			'style'   => '',
		),
		'tip_position'  => array(
			'my' => 'top left',
			'at' => 'bottom right',
		),
		'tip_effect'    => array(
			'show' => array(
				'effect'   => 'slide',
				'duration' => '500',
				'event'    => 'mouseover',
			),
			'hide' => array(
				'effect'   => 'slide',
				'duration' => '500',
				'event'    => 'click mouseleave',
			),
		),
	)
);

$args['share_icons'][] = array(
	'url'   => 'https://www.facebook.com/mokshastudio',
	'title' => 'Like us on Facebook',
	'icon'  => 'el-icon-facebook'
);
$args['share_icons'][] = array(
	'url'   => 'https://twitter.com/mokshastudio',
	'title' => 'Follow us on Twitter',
	'icon'  => 'el-icon-twitter'
);
$args['share_icons'][] = array(
	'url'   => 'http://www.linkedin.com/company/moksha-design-studio',
	'title' => 'Find us on LinkedIn',
	'icon'  => 'el-icon-linkedin'
);

Redux::setArgs( $opt_name, $args );

function remove_demo() {
	if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
		remove_filter( 'plugin_row_meta', array(
			ReduxFrameworkPlugin::instance(),
			'plugin_metalinks'
		), null, 2 );

		remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
	}
}
