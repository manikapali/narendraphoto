<?php

/**
 * Post Type creating library for WordPress
 * @author Sachit Tandukar
 * @version 1.1.0
 */
namespace Moksha\Trekking\Monkey;

use \Moksha\PostType as CPT;

/**
 * Class PostType
 * @package Moksha
 */
class PostType {
	private $customPostType;

	public function __construct() {
		$this->init( array( &$this, 'setPostType' ) );
	}

	/**
	 * call function on WordPress init hook
	 *
	 * @param $callback
	 */
	private function init( $callback ) {
		add_action( 'init', $callback, 99 );
	}

	/**
	 * function for setting new post type
	 */
	public function setPostType() {
		$this->customPostType = new CPT();
		$postType             = array();
		$this->customPostType->createPostType( array_merge( $postType, apply_filters( 'mok_custom_post_type', array() ) ) );
		$taxonomy = array();
		$this->customPostType->addTaxonomy( array_merge_recursive( $taxonomy, apply_filters( 'mok_custom_taxonomy', array() ) ) );
	}
}