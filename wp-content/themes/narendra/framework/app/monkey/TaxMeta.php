<?php

namespace Moksha\Trekking\Monkey;

use Moksha\TaxMeta as Meta;

class TaxMeta {

	public function __construct() {
		new Meta( $this->setTaxonomyMeta() );
	}


	/**
	 * Creates Tax Meta by calling Tax Meta class internally
	 */
	public function setTaxonomyMeta() {
		$tax_meta_boxes = array();

		return array_merge_recursive( $tax_meta_boxes, apply_filters( 'mok_add_tax_meta_boxes', array() ) );
	}
}