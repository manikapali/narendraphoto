<?php

// Adds pagination
if ( ! function_exists( 'mok_add_pagination' ) ) {
	function mok_add_pagination() {
		if ( function_exists( 'wp_pagenavi' ) ):
			echo apply_filters( 'mok_pagination_wrap_start', '' );
			wp_pagenavi();
			echo apply_filters( 'mok_pagination_wrap_end', '' );
		else:
			echo apply_filters( 'mok_pagination_wrap_start', '' );
			mok_pagination();
			echo apply_filters( 'mok_pagination_wrap_end', '' );
		endif;

	}
}
add_action( 'mok_pagination', 'mok_add_pagination' );

// Displays breadcrumb
if ( ! function_exists( 'mok_add_breadcrumb' ) ) {
	function mok_add_breadcrumb() {
		if ( ! is_front_page() && function_exists( 'bcn_display' ) ):
			echo apply_filters( 'mok_breadcrumb_wrap_start', '<div class="breadcrumbs">' );
			bcn_display();
			echo apply_filters( 'mok_breadcrumb_wrap_end', '</div>' );
		endif;
	}
}
add_action( 'mok_start_main_content', 'mok_add_breadcrumb' );