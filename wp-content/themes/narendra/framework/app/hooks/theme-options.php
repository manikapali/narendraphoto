<?php
global $mok_options;
$mok_options = get_option( 'moksha_option' );
// Add google analytics code before head end tag
if ( ! function_exists( 'mok_get_tracking_code' ) ) {
	/**
	 * Adds google analytics code based on tracking code
	 */
	function mok_get_tracking_code() {
		global $mok_options;
		?>
		<!--Google Analytics code-->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', '<?php echo esc_html($mok_options['mok-tracking-code']); ?>']);
			_gaq.push(['_trackPageview']);
			(function () {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<?php
	}
}
add_action( 'mok_before_head_end', 'mok_get_tracking_code' );

/**
 * Get logo of website
 */
add_action( 'mok_logo', 'mok_get_logo' );

/**
 * Get Welcome message
 */
add_action( 'mok_welcome_msg', 'mok_get_welcome_text' );

// Displays social account
if ( ! function_exists( 'mok_display_social_account_lists' ) ) {
	/**
	 * Displays social contact lists
	 */
	function mok_display_social_account_lists() {
		$fb_url = mok_get_social_url( 'fb' );
		$tw_url = mok_get_social_url( 'tw' );
		$ln_url = mok_get_social_url( 'ln' );
		$pt_url = mok_get_social_url( 'pt' );
		if ( $fb_url !== '' || $tw_url !== '' || $ln_url !== '' || $pt_url !== '' ):
			echo apply_filters( 'mok_social_wrap_start', '' );
			?>
			<?php if ( $fb_url !== '' ): ?>
			<li><a class="<?php echo apply_filters( 'mok_social_fb_class', '' ); ?>"
			       href="<?php echo $fb_url; ?>"
			       target="_blank"><?php echo apply_filters( 'mok_social_fb_icon', '' ); ?></a></li>
		<?php endif; ?>
			<?php if ( $tw_url !== '' ): ?>
			<li><a class="<?php echo apply_filters( 'mok_social_tw_class', '' ); ?>"
			       href="<?php echo $tw_url; ?>"
			       target="_blank"><?php echo apply_filters( 'mok_social_tw_icon', '' ); ?></a></li>
		<?php endif; ?>
			<?php if ( $ln_url !== '' ): ?>
			<li><a class="<?php echo apply_filters( 'mok_social_ln_class', '' ); ?>"
			       href="<?php echo $ln_url; ?>"
			       target="_blank"><?php echo apply_filters( 'mok_social_ln_icon', '' ); ?></a></li>
		<?php endif; ?>
			<?php if ( $pt_url !== '' ): ?>
			<li><a class="<?php echo apply_filters( 'mok_social_pt_class', '' ); ?>"
			       href="<?php echo $pt_url; ?>"
			       target="_blank"><?php echo apply_filters( 'mok_social_pt_icon', '' ); ?></a></li>
		<?php endif; ?>
			<?php
			echo apply_filters( 'mok_social_wrap_end', '' );
		endif;
	}
}
add_action( 'mok_social_account', 'mok_display_social_account_lists' );

// Displays contact information
if ( ! function_exists( 'mok_display_contact_info' ) ) {
	/**
	 * Displays contact information
	 */
	function mok_display_contact_info() {
		$contact_po_box   = mok_get_contact_info( 'po-box' );
		$contact_address  = mok_get_contact_info( 'address' );
		$contact_country  = mok_get_contact_info( 'country' );
		$contact_locality = mok_get_contact_info( 'locality' );
		$contact_tel      = mok_get_contact_info( 'tel' );
		$contact_fax      = mok_get_contact_info( 'fax' );
		$contact_mobile   = mok_get_contact_info( 'mobile' );
		$contact_email    = mok_get_contact_info( 'email' );
		if ( $contact_po_box !== '' || $contact_address !== '' || $contact_mobile !== '' || $contact_tel !== '' || $contact_email !== '' || $contact_country !== '' || $contact_locality !== '' ):
			echo apply_filters( 'mok_contact_info_wrap_start', '' );
			?>
			<?php if ( $contact_address !== '' ): ?>
			<p><?php echo $contact_address; ?></p>
		<?php endif; ?>
			<?php if ( $contact_locality !== '' ): ?>
			<p><?php echo $contact_locality; ?></p>
		<?php endif; ?>
			<?php if ( $contact_po_box !== '' ): ?>
			<p>G.P.O: <?php echo $contact_po_box; ?></p>
		<?php endif; ?>
			<?php if ( $contact_country !== '' ): ?>
			<p><?php echo $contact_country; ?></p>
		<?php endif; ?>
			<?php if ( $contact_mobile !== '' ): ?>
			<strong><?php echo $contact_mobile; ?></strong>
		<?php endif; ?>
			<?php if ( $contact_tel !== '' ): ?>
			<strong><?php echo $contact_tel; ?></strong>
		<?php endif; ?>
			<?php if ( $contact_fax !== '' ): ?>
			<p><?php echo $contact_fax; ?></p>
		<?php endif; ?>
			<?php if ( $contact_email !== '' || mok_display_social_account_lists() !== '' ): ?>
			<p>Or find me in these places</p>
			<div class="social-links">
				<ul>
					<li><a href="mailto:<?php echo $contact_email; ?>" class="btn btn-social icon-mail">
							<?php echo $contact_email; ?></a></li>
					<?php do_action( 'mok_social_account' ); ?>
				</ul>
			</div>
		<?php endif; ?>
			<?php
			echo apply_filters( 'mok_contact_info_wrap_end', '' );
		endif;
	}
}
add_action( 'mok_contact_info', 'mok_display_contact_info' );

// Displays banner on home page and inner page
if ( ! function_exists( 'mok_display_banner' ) ) {
	/**
	 * Displays banner on home page and inner page
	 */
	function mok_display_banner() {
		?>
		<div id="banner">
			<?php echo mok_get_banners(); ?>
		</div>
		<?php
	}
}
add_action( 'mok_before_main_start', 'mok_display_banner' );