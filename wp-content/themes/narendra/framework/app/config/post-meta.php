<?php

if ( ! function_exists( 'mok_alter_meta_boxes' ) ) {
	/**
	 * @return array
	 */
	function mok_alter_meta_boxes() {
		return array(
			'_gallery_images'    => array(
				'id'           => '_gallery_images',
				'title'        => __( 'Images', 'mok' ),
				'object_types' => array( 'gallery' ),
				'context'      => 'normal',
				'priority'     => 'low',
				'fields'       => array(
					array(
						'name' => __( 'Gallery Images', '_mok' ),
						'id'   => '_gallery_images_list',
						'type' => 'file_list',
					)
				)
			),
			'_stories_images'    => array(
				'id'           => '_stories_images',
				'title'        => __( 'Images', 'mok' ),
				'object_types' => array( 'stories' ),
				'context'      => 'normal',
				'priority'     => 'low',
				'fields'       => array(
					array(
						'name' => __( 'Stories Images', '_mok' ),
						'id'   => '_stories_images_list',
						'type' => 'file_list',
					)
				)
			),
			'_billboard_details' => array(
				'id'           => '_billboard_details',
				'title'        => __( 'Images', 'mok' ),
				'object_types' => array( 'billboard' ),
				'context'      => 'normal',
				'priority'     => 'low',
				'fields'       => array(
					array(
						'name' => __( 'Image Detail Link', '_mok' ),
						'id'   => '_billboard_images_detail',
						'type' => 'text',
					),
					array(
						'name' => __( 'Gallery Image Link', '_mok' ),
						'id'   => '_billboard_gallery_link',
						'type' => 'text',
					)
				)
			)
		);
	}
}
add_filter( 'mok_add_post_meta_boxes', 'mok_alter_meta_boxes' );
