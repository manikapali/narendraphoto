<?php

$opt_name = "moksha_option";

// Change the arguments after they've been declared, but before the panel is created
add_filter( 'redux/options/' . $opt_name . '/args', 'change_arguments' );

// Dynamically add a section. Can be also used to modify sections/fields
add_filter( 'redux/options/' . $opt_name . '/sections', 'dynamic_section' );


function dynamic_section( $sections ) {

	ob_start();

	$theme      = wp_get_theme();
	$screenshot = $theme->get_screenshot();
	$class      = $screenshot ? 'has-screenshot' : '';

	$customize_title = sprintf( __( 'Customize &#8220;%s&#8221;', '_mok' ), $theme->display( 'Name' ) );

	?>
	<div id="current-theme" class="<?php echo esc_attr( $class ); ?>">
		<?php if ( $screenshot ) : ?>
			<?php if ( current_user_can( 'edit_theme_options' ) ) : ?>
				<a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize"
				   title="<?php echo esc_attr( $customize_title ); ?>">
					<img src="<?php echo esc_url( $screenshot ); ?>"
					     alt="<?php esc_attr_e( 'Current theme preview', '_mok' ); ?>"/>
				</a>
			<?php endif; ?>
			<img class="hide-if-customize" src="<?php echo esc_url( $screenshot ); ?>"
			     alt="<?php esc_attr_e( 'Current theme preview', '_mok' ); ?>" width="250"/>
		<?php endif; ?>

		<h4><?php echo $theme->display( 'Name' ); ?></h4>

		<div>
			<ul class="theme-info">
				<li><?php printf( __( 'By %s', '_mok' ), $theme->display( 'Author' ) ); ?></li>
				<li><?php printf( __( 'Version %s', '_mok' ), $theme->display( 'Version' ) ); ?></li>
				<li><?php echo '<strong>' . __( 'Tags', '_mok' ) . ':</strong> '; ?><?php printf( $theme->display( 'Tags' ) ); ?></li>
			</ul>
			<p class="theme-description"><?php echo $theme->display( 'Description' ); ?></p>
		</div>
	</div>

	<?php
	$item_info = ob_get_contents();

	ob_end_clean();
	$sections[] = array(
		'icon'   => 'el-icon-cogs',
		'title'  => __( 'General Settings', '_mok' ),
		'fields' => array(
			array(
				'id'       => 'mok-logo',
				'type'     => 'media',
				'url'      => false,
				'title'    => __( 'Logo', '_mok' ),
				'subtitle' => __( 'Upload your website logo', '_mok' ),
				'hint'     => array(
					'content' => 'This is a logo for website'
				)
			),
			array(
				'id'       => 'mok-tracking-code',
				'type'     => 'text',
				'title'    => __( 'Tracking Code', '_mok' ),
				'subtitle' => __( 'Paste your Google Analytics (or other) tracking code here. Add account number such as UA-XXXXXXXX-X', '_mok' ),
				'validate' => 'no_html',
				'hint'     => array(
					'content' => 'Add account number such as UA-24244411-1'
				)
			)
		)
	);
	$sections[] = array(
		'title'  => __( 'Home Settings', '_mok' ),
		'icon'   => 'el-icon-home',
		'fields' => array(
			array(
				'id'       => 'mok-welcome',
				'type'     => 'editor',
				'title'    => __( 'Welcome/ Intro Message', '_trek' ),
				'desc'     => __( '', '_trek' ),
				'subtitle' => __( 'Your welcome message would go here.', '_trek' ),
				'args'     => array(
					'media_buttons' => false,
					'wpautop'       => true
				)
			)
		)
	);
	$sections[] = array(
		'icon'   => 'el-icon-facebook',
		'title'  => __( 'Social Media Settings', '_mok' ),
		'fields' => array(
			array(
				'id'       => 'mok-fb',
				'type'     => 'text',
				'title'    => __( 'Facebook Page URL', '_mok' ),
				'subtitle' => __( 'Enter your facebook page URL', '_mok' ),
				'validate' => 'link'
			),
			array(
				'id'       => 'mok-tw',
				'type'     => 'text',
				'title'    => __( 'Twitter Profile URL', '_mok' ),
				'subtitle' => __( 'Enter your twitter profile URL', '_mok' ),
				'validate' => 'link'
			),
			array(
				'id'       => 'mok-ln',
				'type'     => 'text',
				'title'    => __( 'LinkedIn Profile URL', '_mok' ),
				'subtitle' => __( 'Enter your linkedin profile URL', '_mok' ),
				'validate' => 'link'
			),
			array(
				'id'       => 'mok-pt',
				'type'     => 'text',
				'title'    => __( 'Pinterest Profile URL', '_mok' ),
				'subtitle' => __( 'Enter your pinterest profile URL', '_mok' ),
				'validate' => 'link'
			)
		)
	);
	$sections[] = array(
		'icon'   => 'el-icon-address-book',
		'title'  => __( 'Contact Information', '_mok' ),
		'fields' => array(
			array(
				'id'       => 'mok-email',
				'type'     => 'text',
				'title'    => __( 'E-mail address', '_mok' ),
				'subtitle' => __( 'Enter your e-mail address', '_mok' ),
				'validate' => 'email',
				'msg'      => 'Enter Valid email address'
			),
			array(
				'id'       => 'mok-po-box',
				'type'     => 'text',
				'title'    => __( 'P.O. Box Number', '_mok' ),
				'subtitle' => __( 'Enter your P.O. Box Number', '_mok' ),
				'validate' => 'no_html'
			),
			array(
				'id'       => 'mok-tel',
				'type'     => 'text',
				'title'    => __( 'Telephone Number', '_mok' ),
				'subtitle' => __( 'Enter your telephone number', '_mok' ),
				'validate' => 'no_html'
			),
			array(
				'id'       => 'mok-fax',
				'type'     => 'text',
				'title'    => __( 'Fax Number', '_mok' ),
				'subtitle' => __( 'Enter your fax number', '_mok' ),
				'validate' => 'no_html'
			),
			array(
				'id'       => 'mok-mobile',
				'type'     => 'text',
				'title'    => __( 'Mobile Number', '_mok' ),
				'subtitle' => __( 'Enter your mobile number', '_mok' ),
				'validate' => 'no_html'
			),
			array(
				'id'       => 'mok-address',
				'type'     => 'textarea',
				'title'    => __( 'Street Address', '_mok' ),
				'subtitle' => __( 'Enter your contact address', '_mok' ),
				'validate' => 'no_html'
			),
			array(
				'id'       => 'mok-locality',
				'type'     => 'text',
				'title'    => __( 'City', '_mok' ),
				'subtitle' => __( 'Enter your city name', '_mok' ),
				'validate' => 'no_html'
			),
			array(
				'id'       => 'mok-country',
				'type'     => 'text',
				'title'    => __( 'Country', '_mok' ),
				'subtitle' => __( 'Enter your country name', '_mok' ),
				'validate' => 'no_html'
			)
		)
	);
	$sections[] = array(
		'title'  => __( 'Import / Export', '_mok' ),
		'desc'   => __( 'Import and Export your website settings from file, text or URL.', '_mok' ),
		'icon'   => 'el-icon-refresh',
		'fields' => array(
			array(
				'id'         => 'opt-import-export',
				'type'       => 'import_export',
				'title'      => 'Import Export',
				'subtitle'   => 'Save and restore your website options',
				'full_width' => false,
			),
		),
	);

	$sections[] = array(
		'type' => 'divide',
	);

	$sections[] = array(
		'icon'   => 'el-icon-wordpress',
		'title'  => __( 'Theme Information', '_mok' ),
		'fields' => array(
			array(
				'id'      => 'opt-raw-info',
				'type'    => 'raw',
				'content' => $item_info,
			)
		),
	);

	return $sections;
}


function change_arguments( $args ) {
	return $args;
}