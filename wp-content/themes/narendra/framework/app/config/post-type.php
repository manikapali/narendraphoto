<?php

if ( ! function_exists( 'mok_alter_post_type' ) ) {
	/**
	 * @return array
	 */
	function mok_alter_post_type() {
		return array(
			'Gallery'   => array(
				'show_in_nav_menus' => false,
				'menu_icon'         => 'dashicons-format-gallery',
				'supports'          => array( 'title', 'thumbnail' ),
				'has_archive'       => true
			),
			'Stories'   => array(
				'show_in_nav_menus' => false,
				'menu_icon'         => 'dashicons-media-text',
				'supports'          => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
				'has_archive'       => true
			),
			'Billboard' => array(
				'show_in_nav_menus' => false,
				'menu_icon'         => 'dashicons-pressthis',
				'supports'          => array( 'title', 'thumbnail' ),
				'has_archive'       => false
			)
		);
	}
}
add_filter( 'mok_custom_post_type', 'mok_alter_post_type' );

if ( ! function_exists( 'mok_alter_taxonomy' ) ) {
	function mok_alter_taxonomy() {
		return array();
	}
}

add_filter( 'mok_custom_taxonomy', 'mok_alter_taxonomy' );
