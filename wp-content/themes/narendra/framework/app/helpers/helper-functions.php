<?php

use Moksha\lib\Taxonomy_MetaData;

// Display file size from the passed linked to file
if ( ! function_exists( 'mok_get_file_size' ) ) {
	/**
	 * Display file size from the passed linked to file
	 *
	 * @param $file
	 * @param int $decimals
	 *
	 * @return string
	 */
	function mok_get_file_size( $file, $decimals = 2 ) {
		$response = wp_remote_get( $file );
		$bytes    = (int) $response['headers']['content-length'];
		$sz       = 'BKMGTP';
		$factor   = floor( ( strlen( $bytes ) - 1 ) / 3 );

		return sprintf( "%.{$decimals}f", $bytes / pow( 1024, $factor ) ) . @$sz[ $factor ];
	}
}


// Tax Meta function for easy data access
if ( ! function_exists( 'get_tax_meta' ) ) {
	/**
	 * Tax Meta function  for easy data access
	 *
	 * @param $taxonomy string Taxonomy Name
	 * @param $term_id int Term id
	 * @param $key string Meta Key
	 *
	 * @return mixed
	 */
	function get_tax_meta( $taxonomy, $term_id, $key ) {
		return Taxonomy_MetaData::get( $taxonomy, $term_id, $key );
	}
}