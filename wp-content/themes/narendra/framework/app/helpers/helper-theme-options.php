<?php

//  Get theme options global variable
global $mok_options;
$mok_options = get_option( 'moksha_option' );

// Get logo of website
if ( ! function_exists( 'mok_get_logo' ) ) {
	/**
	 * Get logo of website
	 * @return string
	 */
	function mok_get_logo() {
		global $mok_options;
		if ( ! empty( $mok_options['mok-logo']['url'] ) && isset( $mok_options['mok-logo']['url'] ) ):
			echo wp_get_attachment_image( $mok_options['mok-logo']['id'], 'full' );
		endif;

		return '';
	}
}

// Get welcome message for website
if ( ! function_exists( 'mok_get_welcome_text' ) ) {
	/**
	 * Get welcome message for website
	 * @return string
	 */
	function mok_get_welcome_text() {
		global $mok_options;

		echo wpautop( nl2br( wp_kses_post( $mok_options['mok-welcome'] ) ) );
	}
}

// Get Banner for Home Page
if ( ! function_exists( 'mok_get_banners' ) ) {
	/**
	 * Get Banner for Home Page
	 * @return string
	 */
	function mok_get_banners() {
		global $mok_options;
		$count = count( $mok_options['mok-slides'] );
		if ( $count && isset( $mok_options['mok-slides'] ) ):
			$banner = '';
			for ( $i = 0; $i < $count; $i ++ ):
				$banner .= '<div><div class="pattern-overlayer">' . wp_get_attachment_image( $mok_options['mok-slides'][ $i ]['attachment_id'], 'full' ) . '</div></div>';
			endfor;
			$banner .= '';

			echo $banner;
		endif;

		echo '';
	}
}

// Get Banner for Inner Page
if ( ! function_exists( 'mok_get_inner_banners' ) ) {
	/**
	 * Get Banner for Inner Page
	 * @return string
	 */
	function mok_get_inner_banners() {
		global $mok_options;
		$count = count( $mok_options['mok-inner-slides'] );
		if ( $count && isset( $mok_options['mok-inner-slides'] ) ):
			$i = mt_rand( 0, $count - 1 );

			echo wp_get_attachment_image( $mok_options['mok-inner-slides'][ $i ]['attachment_id'], 'full' );
		endif;

		return '';
	}
}

// Get social sites URL
if ( ! function_exists( 'mok_get_social_url' ) ) {
	/**
	 * Get social sites URL
	 * Accepted values 'fb', 'gplus', 'tw','ln','pt'
	 *
	 * @param string $social_name
	 *
	 * @return string
	 */
	function mok_get_social_url( $social_name ) {
		global $mok_options;
		$accepted_values = array( 'fb', 'tw', 'ln', 'pt' );
		if ( in_array( $social_name, $accepted_values, true ) ):
			return esc_url( $mok_options[ 'mok-' . $social_name ] );
		endif;

		return '';
	}
}

// Get Contact Information
if ( ! function_exists( 'mok_get_contact_info' ) ) {
	/**
	 * Get Contact Information
	 * Accepted values 'email', 'po-box', 'tel', 'mobile', 'address', 'locality', 'country', 'fax'
	 *
	 * @param string $contact_name
	 *
	 * @return string
	 */
	function mok_get_contact_info( $contact_name ) {
		global $mok_options;
		$accepted_values = array( 'email', 'po-box', 'tel', 'mobile', 'address', 'locality', 'country', 'fax' );
		if ( in_array( $contact_name, $accepted_values, true ) && ! empty( $mok_options[ 'mok-' . $contact_name ] ) ):
			if ( $contact_name === 'tel' || $contact_name === 'mobile' ):
				return ( $mok_options[ 'mok-' . $contact_name ] );
			endif;
			if ( $contact_name === 'email' ):
				return ( antispambot( sanitize_email( $mok_options['mok-email'] ) ) );
			endif;

			return ( $contact_name !== 'address' ) ? esc_html( $mok_options[ 'mok-' . $contact_name ] ) : esc_textarea( $mok_options['mok-address'] );
		endif;

		return '';
	}
}

