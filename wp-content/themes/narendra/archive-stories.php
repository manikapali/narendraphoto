<?php
/**
 * Created by PhpStorm.
 * User: Samit
 * Date: 8/4/2015
 * Time: 10:03 AM
 */
get_header();
?>
<div class="stories-gallery gallery-block">
	<div class="container clearfix">
		<h1>Stories</h1>

		<div class="img-list-block">
			<?php
			if ( have_posts() ):
				?>
				<ul class="img-category-block clearfix">
					<?php while ( have_posts() ):
						the_post();
						?>
						<li class="img-thumb stories-thumb">
							<?php if ( has_post_thumbnail() ): ?>
								<div class="img-holder">
									<?php
									the_post_thumbnail( array( 277, 285 ) );
									?>
									<div class="overlay-transp">
										<a href="<?php the_permalink(); ?>" class="hover-icon icon-plus"></a>
										<a href="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' )[0]; ?>"
										   data-lightbox="image-1"
										   class="hover-icon icon-search fancybox"></a>
									</div>
								</div>
							<?php endif; ?>
							<div class="stories-trailer">
								<h2 class="story-title"><?php the_title(); ?></h2>
								<?php the_excerpt(); ?>
							</div>
						</li>
					<?php endwhile;
					wp_reset_postdata();
					?>
				</ul>
			<?php endif; ?>
			<!--<span class="view-detail"><a href="gallery-detail.html">View More</a></span>-->
		</div>
		<!-- img-list-block ends-->
	</div>
</div>
<!-- gallery-block ends -->
<?php get_footer(); ?>
