<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'moksha_nsdb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_|3q@1DW}rZE ct|7^6;9|w*$>+YDTw|IdD*${/]{koe-xA*`%N;9?;:fQ)T|VyL');
define('SECURE_AUTH_KEY',  '<FGv30mfqk%(EnpRf5*Z*p=CFOiiZ0=7TCQ3zNy3WQp~Ciwjk2<k*Xuih}^UK`0A');
define('LOGGED_IN_KEY',    'sPM1^B7#asUf9/]4.B=yI(<*($Q3e}oP||mZ-Iu|R7/Yh3^+jf2AM^T3qW{WED6%');
define('NONCE_KEY',        'tk<|w)-r(D*-WaGDW]cw>1X&8i4gbL<~+#XmDQ;Sal|t-vGo6!&)tg,)--k#Q}]t');
define('AUTH_SALT',        'OWi&~(u9ndv6G8%idHSKVqZ$90RCa4{YBtTOA>#pBS;9VeH0kL~!3p#jMu2[fAEn');
define('SECURE_AUTH_SALT', ';: M$o%;!|eFsn7At$5,1Y]chXqW|@Xn%]$w5%p5W+?m*>2F&[g%QcT3k9#s^J:j');
define('LOGGED_IN_SALT',   '^_jGp9H$1,qUQkzJzb#fLz9_[E2&.VE+nUoLG0L{R#Sn?DFv=#-F!n@?HqaU|kvl');
define('NONCE_SALT',       '(8P$VM?G,V013Iv~zj!3Uc)ERBmlrODHM]jOfLioirI@r?29Uglp!98fz~6*1:n+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'mokns_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
